# Curso Iniciando com Typescript

https://www.udemy.com/iniciando-com-typescript

## Instalando o NodeJs

Acesse o site https://nodejs.org/en/

### Instalando o NodeJs no Ubuntu

`$ sudo apt install nodejs`

Após instalado, verifique a versão pelo terminal

`$ node -v`

## Instalando o npm

`$ sudo apt install npm`

Após instalado, verifique a versão pelo terminal

`$ npm -v`

## Instalando o Typescript

Utilizado para compilar os arquivos typescript para javascript

`npm install -g typescript`

Após instalado, verifique a versão pelo terminal

`$ tsc -v`

## Typescript: Compilando para javascript

`$ tsc NOME_DO_ARQUIVO`

## Typescript: Compilador fica escutando alterações no arquivo

`$ tsc -w`

## NodeJs: Executando o javascript

`$ node NOME_DO_ARQUIVO_JAVASCRIPT`

## Iniciando o arquivo de configuração da biblioteca

`$ npm init`

## Instalando o Webpack

`$ npm install -g webpack`
`$ npm install ts-loader --save-dev`

## Gerando a aplicação depois de fazer a configuração do webpack

`$ npm install typescript --save-dev`
`$ webpack -w`

