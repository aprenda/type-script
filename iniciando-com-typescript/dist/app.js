var linguagem = 'Typescript';
var texto = "Ol\u00E1 " + linguagem;
var idadex = 33;
// let getPerfil = function (nome: string, idade: number, email: string): void {
//     console.log(nome, idade, email)
// }
// arrow functions
// let getPerfil = (nome: string, idade: number, email: string): void => console.log(nome, idade, email)
var getPerfil = function (nome, idade, email) {
    nome = nome.toUpperCase();
    console.log(nome, idade, email);
};
function getIdade(idade) {
    return idade + " anos";
}
getPerfil("Fernando", 38, 'fernando@fernando.com');
var idadeAnos = getIdade(10);
// arrow functions
console.log(texto, idadex);
