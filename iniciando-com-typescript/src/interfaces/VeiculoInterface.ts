interface VeiculoInterface {
    titulo: string
    rodas: number

    getTitulo(): string

    getRodas(): number
}

interface UsuarioInterface {

}

export {VeiculoInterface, UsuarioInterface}