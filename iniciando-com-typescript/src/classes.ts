// class Veiculo {
//     public titulo: string
//
//     constructor(titulo: string) {
//         this.titulo = titulo
//     }
// }
import {Veiculo} from "./classes/Veiculo";
import {Moto} from "./classes/Moto";

let camaro: Veiculo = new Veiculo('Camaro')
let moto: Moto = new Moto()

console.log(camaro.getTitulo())
console.log(moto.getTitulo())