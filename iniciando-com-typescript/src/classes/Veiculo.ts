import {VeiculoInterface} from "../interfaces/VeiculoInterface";

export class Veiculo implements VeiculoInterface {

    constructor(public titulo: string = "honda", public rodas: number = 2) {
        this.titulo = titulo
    }

    getTitulo(): string {
        return this.titulo
    }

    getRodas(): number {
        return this.rodas;
    }
}