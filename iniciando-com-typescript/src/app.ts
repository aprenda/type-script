let linguagem: string = 'Typescript'
let texto: string = `Olá ${linguagem}`
let idadex: number = 33

// let getPerfil = function (nome: string, idade: number, email: string): void {
//     console.log(nome, idade, email)
// }

// arrow functions

// let getPerfil = (nome: string, idade: number, email: string): void => console.log(nome, idade, email)
let getPerfil = (nome: string, idade: number, email: string): void => {
    nome = nome.toUpperCase()
    console.log(nome, idade, email)
}


function getIdade(idade) {
    return idade + " anos";
}

getPerfil("Fernando", 38, 'fernando@fernando.com')

let idadeAnos: string = getIdade(10)


// arrow functions


console.log(texto, idadex)