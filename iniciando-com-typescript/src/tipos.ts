let nome: string = "Ana"
let idade: number = 19
let ehMaior: boolean = false

if (idade > 18) {
    ehMaior = true
}

let lista: string[] = ["verde", "azul"]
let listaDois: Array<number> = [1, 5]

enum Cor {vermelho = 2, azul, laranja = 19}

let c: Cor = Cor.azul

console.log(c)
console.log(Cor[3])
console.log(Cor[19])


let aux: any
aux = "test"
aux = 42
aux = [10, 20]


let arrayAny: any[] = [32, "teste", {}]


function listaNomes(lista: string[]): void {
    console.log(lista)
}