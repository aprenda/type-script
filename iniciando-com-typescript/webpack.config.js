module.exports = {
    entry: "./src/classes.ts",
    output: {
        filename: "./producao/classes.js"
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        loaders: [
            {test: /\.tsx?$/, loader: "ts-loader"}
        ]
    }
}